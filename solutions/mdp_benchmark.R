library(rcraam)
library(readr)
library(glue)
options(readr.num_columns = 0)

discount <- 0.99
print(glue("Discount factor: {discount}"))


files <- c("inventory_100_100_0.csv", "inventory_200_200_0.csv", 
            "inventory_400_400_0.csv", "inventory_10000_100_0.csv")

for(file in files){
    print(glue("****** {file} **********"))

    time.load <- system.time(mdp <- read_csv(file))
    print(glue("Load time: {time.load[3]}"))


    sol <- solve_mdp(mdp, discount, options=list(algorithm="vi"))
    print(glue('Value iteration: {sol$time}, residual: {sol$residual}'))
    #sol <- solve_mdp(mdp, discount, options=list(algorithm="vi_j"))
    #print(glue('Value iteration (j): {sol$time}, residual: {sol$residual}'))
    sol <- solve_mdp(mdp, discount, options=list(algorithm="pi"))
    print(glue('Policy iteration: {sol$time}, residual: {sol$residual}'))
    sol <- solve_mdp(mdp, discount, options=list(algorithm="mpi"))
    print(glue('Modified policy iteration: {sol$time}, residual: {sol$residual}'))
    sol <- solve_mdp(mdp, discount, options=list(algorithm="lp"))
    print(glue('Linear programming: {sol$time}'))
}
