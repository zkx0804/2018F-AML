import numpy as np
from pulp import *
import pandas as pd


class ProjectALP:
    data_mat = None  # Data from CSV
    prob = None  # Problem Model
    model_target = LpMaximize
    alpha = 1 / 1.02
    ep_size = 200  # 200 states in one episode
    f_n = 18  # Number k of features
    phi = []  # features matrix phi: n x k
    R = []  # Cost/Reward: list of n

    r = []  # value function weight vector   k x 1

    def read_data(self, file_path):
        try:
            self.data_mat = pd.read_csv(file_path, dtype=str, header=0)
        except:
            print("Error: Couldn't get data from CSV. Path: ", file_path)
        finally:
            print("Got Data.")

    def get_phi_and_reward(self, index):

        assert (index < 10, "Index must be less than 10.")

        self.phi = np.zeros((self.ep_size, self.f_n))  # phi: n x k

        for i in range(self.ep_size):

            # Fill phi
            for x in range(0, self.f_n):
                self.phi[i][x] = float(self.data_mat.loc[i + 200 * index, :][x + 1])

            # Fill R
            self.R.append(float(self.data_mat.loc[i + 200 * index, :][-1]))

    def solve_sample(self, phi, R):
        # Using n samples.( 200 ep per sample)

        # Init variables
        c_t = np.ones((1, self.ep_size))  # vector: 1 x n
        w_t = LpVariable.dicts("w", list(range(self.f_n)))  # weight w: k x 1 (No upper or lower bound)
        #
        # for i in range(self.f_n):
        #     w_t[i].setInitialValue(i)

        # Init model
        prob = LpProblem("ALP", self.model_target)


        # Objective function
        # cT phi r
        c_phi = c_t @ self.phi
        prob += lpSum(c_phi[0][i] * w_t[i] for i in range(self.f_n))

        # Constraints
        for i in range(self.ep_size - 1):
            prob += R[i] + self.alpha * lpSum(
                [phi[i + 1][j] * w_t[j] for j in range(self.f_n)]
            ) >= lpSum([phi[i][j] * w_t[j] for j in range(self.f_n)])

        prob.solve()

        print("objective=", value(prob.objective))

        w_list = []
        for i in range(self.f_n):
            print("w_%s: %s" % (i, value(w_t[i])))
            w_list.append(value(w_t[i]))

        print("Status", LpStatus[prob.status])
        print("weight w ", w_list)
        return w_list

    def run_computation(self):

        self.get_phi_and_reward(1)
        self.solve_sample(self.phi, self.R)

    def __init__(self, file_path):
        # Read data
        self.read_data(file_path)


if __name__ == "__main__":
    print("start...")

    app = ProjectALP("./project_sample1.csv")
    # print(app.data_mat)

    app.run_computation()

    print("End.")
